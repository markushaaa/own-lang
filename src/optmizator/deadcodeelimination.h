#ifndef DEADCODEELIMINATION_H
#define DEADCODEELIMINATION_H

#include "visitor.h"
#include "parser/ast/assignmentstatement.h"
#include "parser/ast/ifelsestatement.h"
#include "parser/ast/blockstatement.h"
#include "parser/ast/whilestatement.h"
#include "parser/ast/functionstatement.h"
#include "parser/ast/functionexpression.h"
#include "parser/ast/binaryexprassion.h"
#include "parser/ast/conditionalexprassion.h"
#include "parser/ast/variableexpression.h"
#include "parser/ast/unaryexprassion.h"
#include "parser/ast/numberexprassion.h"


class DeadCodeElimination : public Visitor
{
public:
    DeadCodeElimination();

    // Visitor interface
public:
    Node *visit(AssignmentStatement *st);
    Node *visit(IfElseStatement *st);
    Node *visit(BlockStatement *st);
    Node *visit(WhileStatement *st);
    Node *visit(FunctionStatement *st);
    Node *visit(FunctionExpression *st);
    Node *visit(BinaryExprassion *st);
    Node *visit(ConditionalExprassion *st);
    Node *visit(VariableExpression *st);
    Node *visit(UnaryExprassion *st);
};

#endif // DEADCODEELIMINATION_H
