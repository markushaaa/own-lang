#pragma once
#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include <QMessageBox>
#include "lexer/lexer.h"
#include "parser/parser.h"
#include "optmizator/optimazer.h"
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_btnRun_clicked();

private:
    Ui::MainWindow *ui;
    Lexer* lexer;
    Parser* parser;
    Optimazer* optimazer;
};
#endif // MAINWINDOW_H
