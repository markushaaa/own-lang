#include "binaryexprassion.h"


Exprassion *BinaryExprassion::getExpr1() const
{
    return expr1;
}

Exprassion *BinaryExprassion::getExpr2() const
{
    return expr2;
}

void BinaryExprassion::setExpr1(Exprassion *value)
{
    expr1 = value;
}

void BinaryExprassion::setExpr2(Exprassion *value)
{
    expr2 = value;
}

BinaryExprassion::BinaryExprassion(char operation, Exprassion *expr1, Exprassion *expr2){
    this->expr1 = expr1;
    this->expr2 = expr2;
    this->operation = operation;
}

Value* BinaryExprassion::eval(){
    double val1 = expr1->eval()->asNumber();
    double val2 = expr2->eval()->asNumber();
    switch (operation) {
    case '+':{
        return new NumberValue(val1 + val2);
    }
    case '-':{
        return new NumberValue(val1 - val2);
    }

    case '*':{
        return new NumberValue(val1 * val2);
    }

    case '/':{
        return new NumberValue(val1 / val2);
    }

    default:{
        return new NumberValue(val1 + val2);
    }
    }
}

void BinaryExprassion::toString(QTreeWidget* AST,QTreeWidgetItem* father){
    QTreeWidgetItem* operationItem = new QTreeWidgetItem;
    operationItem->setText(0,QString(operation));
    if(father != nullptr){
        father->addChild(operationItem);
    }
    expr1->toString(AST,operationItem);
    expr2->toString(AST,operationItem);
}
