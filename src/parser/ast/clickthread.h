#pragma once
#ifndef CLICKTHREAD_H
#define CLICKTHREAD_H
#include <QThread>
#include <QList>
#include "value.h"
#include <windows.h>
#include <QDebug>
class ClickThread : public QThread
{
private:
    QList<Value*> args;
public:
    ClickThread(QList<Value*> args);



    // QThread interface
protected:
    void run();
};

#endif // CLICKTHREAD_H
