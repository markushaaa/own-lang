#pragma once
#ifndef DRAGFUNCTION_H
#define DRAGFUNCTION_H
#include "functions.h"
#include "function.h"
#include "dragthread.h"


class DragFunction : public Function
{
public:
    DragFunction();

    // Function interface
public:
    Value *execute(QList<Value*> args);
};

#endif // DRAGFUNCTION_H
