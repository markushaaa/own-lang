#pragma once
#ifndef FUNCTION_H
#define FUNCTION_H
#include "value.h"

class Function
{
public:
    Function();
    virtual Value* execute(QList<Value*>) = 0;
};

#endif // FUNCTION_H
