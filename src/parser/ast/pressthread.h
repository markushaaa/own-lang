#pragma once
#ifndef PRESSTHREAD_H
#define PRESSTHREAD_H
#include <QThread>
#include "value.h"
#include "windows.h"
#include <QTimer>
#include <QDebug>
class pressThread : public QThread
{
private:
    QList<Value*> args;
    int size;

public:
    pressThread(QList<Value*> args);

    // QThread interface
    void run();

};

#endif // PRESSTHREAD_H
