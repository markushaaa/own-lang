#pragma once
#ifndef CONDITIONALEXPRASSION_H
#define CONDITIONALEXPRASSION_H
#include "exprassion.h"
class ConditionalExprassion : public Exprassion
{
    Exprassion *expr1,*expr2;
    char operation;
public:
    ConditionalExprassion(char operation,Exprassion* expr1,Exprassion* expr2);

    // Exprassion interface
public:
    Value* eval();
    void toString(QTreeWidget*, QTreeWidgetItem*);

    // Node interface
    Node* accept(Visitor* visitor){
       return visitor->visit(this);
    }
    Exprassion *getExpr1() const;
    Exprassion *getExpr2() const;
    void setExpr1(Exprassion *value);
    void setExpr2(Exprassion *value);
};

#endif // CONDITIONALEXPRASSION_H
