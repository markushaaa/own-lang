#include "value.h"

Value::Value()
{

}

NumberValue::NumberValue(double value){
    this->value = value;
}

double NumberValue::asNumber(){
    return value;
}

QString NumberValue::asString(){
    return QString::number(value);
}

StringValue::StringValue(QString value){
    this->value = value;
}

double StringValue::asNumber(){
    return value.toDouble();
}

QString StringValue::asString(){
    return value;
}
