#include "dragfunction.h"



DragFunction::DragFunction()
{

}

Value *DragFunction::execute(QList<Value *> args){
    if(args.size() < 2){
        throw std::runtime_error("No matching function");
    }
    if(args.size() % 2 != 0){
        throw std::runtime_error("No matching function");
    }
    DragThread* thread = new DragThread(args);
    thread->start();
    return new StringValue("Drag ok");
}
