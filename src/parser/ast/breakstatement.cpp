#include "breakstatement.h"


BreakStatement::BreakStatement()
{

}

void BreakStatement::execute(){
    throw this;
}

void BreakStatement::toString(QTreeWidget *AST, QTreeWidgetItem *father){
    QTreeWidgetItem* breakItem = new QTreeWidgetItem;
    breakItem->setText(0,"break");
    if(father != nullptr){
        father->addChild(breakItem);
        return;
    }
    AST->addTopLevelItem(breakItem);
}
