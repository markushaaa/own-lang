#pragma once
#ifndef ASSIGNMENTSTATEMENT_H
#define ASSIGNMENTSTATEMENT_H

//#include "exprassion.h"
#include "variables.h"
#include "statement.h"
class Visitor;

class AssignmentStatement: public Statement
{
private:
    QString var;
    Exprassion* expr;

public:
    AssignmentStatement(QString var,Exprassion* expr);
    // Statement interface
public:
    void execute();

    // Statement interface
public:
    void toString(QTreeWidget* AST, QTreeWidgetItem* father);

//     Node interface
    Node* accept(Visitor* visitor){
        return visitor->visit(this);
    }
    Exprassion *getExpr() const;
    QString getVar() const;
    void setExpr(Exprassion *value);
};

#endif // ASSIGNMENTSTATEMENT_H
