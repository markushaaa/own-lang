#pragma once
#ifndef FUNCTIONEXPRESSION_H
#define FUNCTIONEXPRESSION_H
#include "exprassion.h"
#include "function.h"
#include <QList>


class FunctionExpression : public Exprassion
{
private:
    QString name;
    QList<Exprassion*> arguments;
public:
    FunctionExpression(QString name);

    FunctionExpression(QString name,QList<Exprassion*> arguments);

    // Exprassion interface
public:

    void addArgument(Exprassion* arg);

    Value* eval();
    void toString(QTreeWidget* AST, QTreeWidgetItem* father);

    // Node interface
    Node* accept(Visitor* visitor){
       return visitor->visit(this);
    }
    QList<Exprassion *> getArguments() const;
    void setArguments(const QList<Exprassion *> &value);
    void setArgument(int i,Exprassion*);

};

#endif // FUNCTIONEXPRESSION_H
